const express = require('express')

const app = express()

app.get('/', (req, res) => {
    res.send("Hello from server!")
})

app.get('/users', (req, res) => {
    res.send(['Max', 'Alex', 'Mary'])
})

// app.listen(3000)
module.exports = app;
